(* $Id$ *)

open Uq_engines.Operators
open Netimap_prelim

type rd_state =
    { buf : string;
      mutable pos : int;
      mutable dquote_buf : Buffer.t option;
      mutable literal_buf : 
                (int64 * literal * Netchannels.out_obj_channel) option;
      temp_dir : string;
    }

(* read_e: gets input from io, and split it into tokens, which are then
   added to token_q. Also:
    - Concatenates adjacent QText tokens
    - Reads literals in

   read_e finishes first when all data until EOF has been read in
 *)


(* TODO: also put all literals somewhere, so we can delete the files at
   some point again
 *)

let from_sub_string s pos len =
  let k = ref pos in
  Lexing.from_function
    (fun s_lex n ->
       let available = len - !k in
       let p = min n available in
       String.blit s !k s_lex 0 p;
       p
    )



let read_e state token_q (io : Uq_io.in_device) esys =
  let rec read_loop_e () =
    let buf = state.buf in
    (Uq_io.input_e io (`String buf) state.pos (String.length buf - state.pos)
       >> Uq_io.eof_as_none
    )
    ++ (fun n_opt ->
          let len =
            match n_opt with
              | None -> state.pos
              | Some n -> state.pos + n in
          match state.literal_buf with
            | None ->
                 split_into_tokens_e (n_opt=None) len
            | Some(to_go, lit, ch) ->
                 literal_loop_e (n_opt=None) len to_go lit ch
       )

    and split_into_tokens_e eof len =
      (* Use the lexer to split the input into tokens. Scanning always starts
         at the beginning of buf (NOT at state.pos)
       *)
      let buf = state.buf in
      let lexbuf = from_sub_string buf 0 len in
      let cont = ref `Continue in
      while !cont = `Continue do
        let token =
          if state.dquote_buf <> None then
            Netimap_lexer.dquoted lexbuf
          else
            Netimap_lexer.token lexbuf in
        match token with
          | End ->
               cont := `Stop
          | Incomplete ->
               cont := `Stop;
               if Lexing.lexeme_start lexbuf = 0 then (
                 Queue.add (Error "Token too long") token_q
               )
          | Literal_header s ->
               ( try
                   let n = Int64.of_string s in
                   if n > 0xffff_ffffL then
                     failwith "Integer too big";
                   cont := `Literal n
                 with
                   | Failure _ ->
                        Queue.add (Error "Integer too big") token_q;
                        cont := `Stop
               )
          | Dquote ->
               ( match state.dquote_buf with
                   | None ->
                        state.dquote_buf <- Some(Buffer.create 80)
                   | Some b ->
                        Queue.add (QText (Buffer.contents b)) token_q;
                        state.dquote_buf <- None
               )
          | QText s ->
               ( match state.dquote_buf with
                   | None ->
                        assert false
                   | Some b ->
                        Buffer.add_string b s
               )
          | _ -> Queue.add token token_q
      done;
      ( match !cont with
          | `Stop ->
               (* hit the end of the lexbuf *)
               let p = Lexing.lexeme_start lexbuf in
               if p > 0 then
                 String.blit buf p buf 0 (len - p);
               let pos1 =
                 if p > 0 then
                   len - p
                 else 
                   0 in
               state.pos <- pos1;
               if eof && pos1 > 0 then
                 Queue.add
                   (Error "Incomplete token at end of data stream")
                   token_q;
               eps_e (`Done ()) esys
          | `Literal to_go ->
               (* found a literal *)
               state.pos <- Lexing.lexeme_end lexbuf;
               let lit, ch = Netimap_prelim.create_literal state.temp_dir in
               literal_loop_e eof len to_go lit ch
          | `Continue ->
               assert false
      )

    and literal_loop_e eof len to_go lit ch =
      (* Store the literal away. This starts at state.pos! *)
      if to_go > 0L then (
        let n = Int64.to_int (min to_go (Int64.of_int (len - state.pos))) in
        ch # really_output state.buf state.pos n;
        state.pos <- state.pos + n;
        let to_go1 = Int64.sub to_go (Int64.of_int n) in
        state.literal_buf <- Some(to_go1, lit, ch);
        if state.pos = len then (
          state.pos <- 0;
          if eof then (
            Queue.add
              (Error "Incomplete literal at end of data stream")
              token_q;
            eps_e (`Done ()) esys
          )
          else
            read_loop_e()
        )
        else
          literal_loop_e eof len to_go1 lit ch
      )
      else (
        ch # close_out();
        Queue.add (Literal lit) token_q;
        let len1 = len - state.pos in
        String.blit state.buf state.pos state.buf 0 len1;
        split_into_tokens_e eof len1
      )
  in
  read_loop_e()


let rd_state temp_dir =
  { buf = String.create 65536;
    pos = 0;
    dquote_buf = None;
    literal_buf = None;
    temp_dir
  }


let cleanup state =
  match state.literal_buf with
    | Some (_, _, ch) -> ch # close_out()
    | _ -> ()
