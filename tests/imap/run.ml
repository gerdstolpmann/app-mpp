(* This must be run from the toploop! *)

#use "topfind";;
#require "netstring,equeue";;
#directory "+compiler-libs";;
#directory "../../src/imap";;
#load "netimap.cma";;

#print_depth 1000;;
#print_length 1000;;

let exec s =
  let lb = Lexing.from_string s in
  let ast = !Toploop.parse_toplevel_phrase lb in
  let buf = Buffer.create 80 in
  let fmt = Format.formatter_of_buffer buf in
  Format.pp_set_margin fmt max_int;
  Format.pp_set_max_indent fmt 1000;
  Format.pp_set_max_boxes fmt 100;
  let ok = Toploop.execute_phrase true fmt ast in
  Format.pp_print_flush fmt ();
  (* if not ok then failwith "run"; *)
  Buffer.contents buf
;;


let error filename n =
  failwith ("Error on line " ^ string_of_int n ^ " in file " ^ filename)


let read_test_data filename =
  let f = open_in filename in
  let l = ref [] in
  let cur = ref None in
  let n = ref 0 in
  ( try
      while true do
        let line = input_line f in
        incr n;
        let clas =
          if line = "" then
            (if !cur = None then `Comment else `Data)
          else
            match line.[0] with
              | '#' when !cur=None -> `Comment
              | '$'-> `Start
              | '.' -> `End
              | _ -> `Data in
        match clas with
          | `Comment -> ()
          | `Start ->
               if !cur<>None then error filename !n;
               let name = String.sub line 1 (String.length line-1) in
               let buf = Buffer.create 80 in
               cur := Some(name,buf)
          | `End ->
               ( match !cur with
                   | None -> error filename !n
                   | Some(name,buf) -> 
                        l := (name,Buffer.contents buf) :: !l;
                        cur := None
               )
          | `Data ->
               ( match !cur with
                   | None -> error filename !n
                   | Some(name,buf) ->
                        if Buffer.length buf > 0 then
                          Buffer.add_char buf '\n';
                        Buffer.add_string buf line
               )
      done
    with
      | End_of_file -> ()
  );
  close_in f;
  List.rev !l


let data = ref "" ;;

let single_test () =
  Netimap_response.test_s
    ~auth_in_progress:false
    !data


let run_test (name,s) =
  data := s;
  let report = exec "single_test();;" in
  print_endline ("Test " ^ name ^ ":\n" ^ report)

