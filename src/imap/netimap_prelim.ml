(* $Id$ *)

open Printf

class type literal =
  object
    inherit Netmime.mime_body

    (* method as_stream : unit -> Netstream.in_obj_stream *)
  end


type token =
  | Lparen
  | Rparen
  | Asterisk
  | Percent
  | Space
  | BS
  | Dquote
  | Literal_header of string
  | Literal of literal
  | Control of char
  | Token of string * string   (* second string is uppercased *)
  | QText of string
  | CRLF
  | Incomplete
  | End                 (* of some token list *)
  | EOF
  | Error of string

(* NB. Token = atom including "]" *)

type msg_flag =
   [ `Answered | `Flagged | `Deleted | `Seen | `Draft | `System of string
   | `Keyword of string
   ]


type resp_state =
   [ `OK | `NO | `BAD ]

type resp_text_code =
  [ `Alert 
  | `Bad_charset of string list
  | `Parse
  | `Permanentflags of msg_flag list
  | `Read_only
  | `Read_write
  | `Try_create
  | `Uidnext of int64
  | `Uidvalidity of int64
  | `Unseen of int64
  | `Capability of string list
  | `None
  ]
    
type separator = char

type status_att =
   [ `Messages | `Recent | `Uidnext | `Uidvalidity | `Unseen ]

type mailbox =
   string list
     (* Interpreted as [String.concat separator mailbox]. The strings are
        UTF-7-encoded if necessary
      *)

type mailbox_pair =
   string * mailbox option
    (* (unparsed_mailbox, parsed_mailbox) *)


type formal_mbox_address =
   [ `Mailbox of string * string option ]


type display_mbox_address =
   string * formal_mbox_address


type formal_address =
   [ `Group of display_mbox_address list
   | formal_mbox_address
   ]

type display_address =  (* pair of (display_name, rfc822 address) *)
   string * formal_address






type ret_msg_flag =
   [ msg_flag | `Recent ]
   (* = msg_flag plus generated flags *)

type store_flags =
  [ `Set of msg_flag list
  | `Add of msg_flag list
  | `Del of msg_flag list
  ]
    
type mbx_flag =
   [ `Noinferiors | `Noselect | `Marked | `Unmarked | `System of string ]

type seq_number = [ `N of int64 | `Last ]

type seq_range = [ `Range of seq_number * seq_number ]

type seq_set = [ seq_number | seq_range ] list

type search_key =
   [ `All
   | `Answered
   | `Bcc of string
   | `Before of Netdate.t   (* only day/month/year *)
   | `Body of string
   | `Cc of string
   | `Deleted
   | `Flagged
   | `From of string
   | `Keyword of string
   | `New
   | `Old
   | `On of Netdate.t  (* only day/month/year *)
   | `Recent
   | `Seen
   | `Since of Netdate.t  (* only day/month/year *)
   | `Subject of string
   | `Text of string
   | `To of string
   | `Unanswered
   | `Undeleted
   | `Unflagged
   | `Unkeyword of string
   | `Unseen
   | `Draft
   | `Header of string * string
   | `Larger of int64
   | `Not of search_key
   | `Or of search_key * search_key
   | `Sentbefore of Netdate.t  (* only day/month/year *)
   | `Senton of Netdate.t  (* only day/month/year *)
   | `Sentsince of Netdate.t  (* only day/month/year *)
   | `Smaller of int64
   | `Uid of seq_set
   | `Undraft
   | `Seq_set of seq_set
   | `And of search_key_list
   ]

 and search_key_list = search_key list  (* AND-ed *)

type section =
   [ `Header
   | `Header_fields of string list
   | `Header_fields_not of string list
   | `Text
   | `Mime  (* inner leaf header *)
   | `Part  (* the whole part *)
   | `Sub of int * section
   ]

type req_msg_att =
   [ `Envelope
   | `Flags
   | `Internaldate
   | `Rfc822
   | `Rfc822_header
   | `Rfc822_size
   | `Rfc822_text
   | `Bodystructure
   | `Uid
   | `Body of section * (int64 * int64) option
   | `Body_peek of section * (int64 * int64) option
   ]

type fetch_spec =
   [ `All | `Full | `Fast | req_msg_att | `List of req_msg_att list ]



type envelope =
   { env_date : string option;
     env_subject : string option;
     env_from : display_address list;
     env_sender : display_address list;
     env_reply_to : display_address list;
     env_to : display_address list;
     env_cc : display_address list;
     env_bcc : display_address list;
     env_in_reply_to : string option;
     env_msg_id : string option
   }

type body_core =
   { bp_media_type : (string * string * (string * string) list);
     bp_disposition : string * (string * string) list; (* Content-Disposition *)
     bp_language : string list;                        (* Content-Language *)
     bp_location : string option;                      (* Content-Location *)
   }

type body =
   { bp_core : body_core;
     bp_id : string option;             (* Content-ID *)
     bp_description : string option;    (* Content-Description *)
     bp_encoding : string;              (* Content-Encoding *)
     bp_md5 : string option;            (* Content-MD5 *)
     bp_octets : int64;
     bp_lines : int64 option;
     bp_envelope : envelope option;
     (* bp_inner_structure : body_structure option; *)
   }

type body_structure =
   [ `Body of body
   | `Parts of body_core * body_structure list
   ]

type msg_att =
   [ `Flags of ret_msg_flag list
   | `Envelope of envelope
   | `Internaldate of Netdate.t
   | `Rfc822 of literal option
   | `Rfc822_header of literal option
   | `Rfc822_text of literal option
   | `Rfc822_size of int64
   | `Bodystructure of body_structure
   | `Body of section * int64 option * literal option
   | `Uid of int64
   ]

 (* TODO: use this address type also in Netaddress *)

type command =
  [ `Capability
  | `Logout
  | `Noop
  | `Login of (* userid *) string * (* password *) string
  | `Authenticate of (* type *) string * (* decoded data *) string
  | `Authenticate_continue of string    (* decoded *)
  | `Starttls
  | `Append of mailbox * msg_flag list * Netdate.t option * literal
  | `Create of mailbox
  | `Delete of mailbox
  | `Examine of mailbox
  | `List of mailbox * mailbox (* the second may contain wildcards *)
  | `List_separator    (* special form of LIST to get the mailbox separator *)
  | `Lsub of mailbox * mailbox (* the second may contain wildcards *)
  | `Rename of mailbox * mailbox
  | `Select of mailbox
  | `Status of mailbox * status_att list  (* >= 1 element *)
  | `Subscribe of mailbox
  | `Unsubscribe of mailbox
  | `Check
  | `Expunge
  | `Search of (* charset *) string option * search_key_list
  | `Uid_search of (* charset *) string option * search_key_list
  | `Fetch of seq_set * fetch_spec
  | `Uid_fetch of seq_set * fetch_spec
  | `Store of seq_set * store_flags * (* silent *) bool
  | `Uid_store of seq_set * store_flags * (* silent *) bool
  | `Copy of seq_set * mailbox
  | `Uid_copy of seq_set * mailbox
  | `Close
  ]

type response_cond =
  [ `State of resp_state * resp_text_code * string
  | `Bye of resp_text_code * string
  | `Preauth of resp_text_code * string
  ]
    
type response_cond_or_eof =
  [ `EOF
  | response_cond
  ]
    
type untagged_response =
  [ `Authenticate_continue of string   (* decoded *)
  | `Command_continue of string
  | `Flags of msg_flag list
  | `List of mbx_flag list * separator option * mailbox
  | `Lsub of mbx_flag list * separator option * mailbox
  | `Search of int64 list
  | `Status of mailbox_pair * (status_att * int64) list
  | `Exists of int64
  | `Recent of int64                                      
  | `Expunge of int64
  | `Fetch of int64 * msg_att list
  | `Capability of string list
  | response_cond
  ]
    
type response =
   { r_untagged : untagged_response list;
     r_tag : string;    (* only meaningful if r_state <> `EOF *)
     r_state : response_cond_or_eof;
   }



let create_literal (dir : string) : (literal * Netchannels.out_obj_channel) =
  let (name, ch) =
    Filename.open_temp_file
      ~temp_dir:dir
      "netimap_"
      ".data" in
  let och =
    new Netchannels.output_channel ch  in
  let body =
    new Netmime.file_mime_body ~ro:true ~fin:true name in
  (body, och)


let inline_literal s : literal =
  new Netmime.memory_mime_body ~ro:true s


let ascii_uppercase s =
  let u = String.copy s in
  for k = 0 to String.length u - 1 do
    match u.[k] with
      | 'a' .. 'z' as c ->
           u.[k] <- Char.uppercase c
      | _ ->
           ()
  done;
  u


let qtext_re = Netstring_str.regexp "[\\\"]"

let quote_string s =
  "\"" ^ 
    Netstring_str.global_substitute
      qtext_re
      (fun m s -> "\\" ^ Netstring_str.matched_string m s)
      s ^ 
      "\""


let mutf7_decode s =
  (* Decode modified UTF-7, and return UTF-8 *)
  let buf = Buffer.create 80 in
  let altbuf = Buffer.create 80 in
  let l = String.length s in
  let rec decode k =
    if k < l then (
      let c = s.[k] in
      match c with
        | '&' ->
             if k+1 < l && s.[k+1] = '-' then (
               Buffer.add_char buf '&';
               decode (k+2)
             )
             else (
               Buffer.clear altbuf;
               decode_b64 (k+1)
             )
        | '\x20'..'\x25'
        | '\x27'..'\x7e' ->
             Buffer.add_char buf c;
             decode (k+1)
        | _ ->
             failwith "mutf7_decode"
    )
  and decode_b64 k =
    if k < l then (
      let c = s.[k] in
      match c with
        | ',' ->
             Buffer.add_char altbuf '/';
             decode_b64 (k+1)
        | '+' | 'A'..'Z' | 'a'..'z' | '0'..'9' ->
             Buffer.add_char altbuf c;
             decode_b64 (k+1)
        | '-' ->
             let n = Buffer.length altbuf in
             let p = n land 3 in
             if p = 1 then failwith "mutf7_decode";
             if p = 2 then Buffer.add_char altbuf '=';
             if p >= 2 then Buffer.add_char altbuf '=';
             let s_utf16 = Netencoding.Base64.decode (Buffer.contents altbuf) in
             let s_utf8 = 
               Netconversion.convert
                 ~in_enc:`Enc_utf16_be ~out_enc:`Enc_utf8 s_utf16 in
             Buffer.add_string buf s_utf8;
             decode (k+1)
        | _ ->
             failwith "mutf7_decode"
      )
    else
      failwith "mutf7_decode" in
  decode 0;
  Buffer.contents buf


