(* $Id$ *)

(* Problems to solve:
   - set recognize_literal when literals are expected (and reset it when
     no longer possible). Also interpret this setting!
 *)


open Netimap_prelim
open Printf

exception Parse_error

type rstate =
    [ `R of response
    | `C of rstate_details * (rstate_details -> token -> rstate)
    ]

and rstate_details =
  { toks : token Queue.t;
    mutable recognize_literal : bool;
    auth_in_progress : bool; 
    (* If set, the parser returns "+" responses as `Authenticate_continue, 
       and not as `Command_continue
     *)
  }

let prefix ?(skip=0) rs n =
  (* return the first up to n tokens *)
  let l = ref [] in
  let k = ref 0 in
  let j = ref 0 in
  try
    Queue.iter
      (fun tok -> 
         if !j >= skip then (
           l := tok :: !l;
           incr k;
           if !k >= n then raise Exit
         );
         incr j
      )
      rs.toks;
    raise Exit
  with
    | Exit -> List.rev !l


let queue_contains rs toks =
  (* FIXME: slow *)
  let n = List.length toks in
  let m = Queue.length rs.toks - n in
  let rec search k =
    if k <= m then
      if prefix ~skip:k rs n = toks then
        Some k
      else
        search (k+1)
    else
      None in
  search 0


let isolate_brackets toks =
  (* Ensures that "[" and "]" are separate tokens.
     This is for parsing [section].
   *)
  let q = ref [] in
  List.iter
    (fun tok ->
       match tok with
         | Token(s,scap) ->
             let k = ref 0 in
             let l = String.length s in
             for j = 0 to l - 1 do
               let c = s.[j] in
               if c = '[' || c = ']' then (
                 if j > !k then (
                   let s1 = String.sub s !k (j - !k) in
                   q := (Token(s1, ascii_uppercase s1)) :: !q;
                   k := j+1
                 );
                 let s2 = String.make 1 c in
                 let tok = Token(s2,s2) in
                 q := tok :: !q;
               )
             done;
             if !k < l then (
               let s1 = String.sub s !k (l - !k) in
               let tok = Token(s1, ascii_uppercase s1) in
               q := tok :: !q;
             )
         | _ ->
             q := tok :: !q;
    )
    toks;
  List.rev !q
  

let isolate_rbracket_space rs =
  (* If there is a token ending in "]" followed by a space, ensure that the
     right bracket is a separate token. (Background: "]" is not a token of
     its own.)

     This is for parsing [resp-text].
   *)
  let q = Queue.create() in
  let prev_tok = ref Incomplete in
  let found = ref false in
  ( try
      Queue.iter
        (fun tok ->
           if !found then
             Queue.add tok q
           else 
             match !prev_tok, tok with
               | (Token(s,scap), Space) when s.[String.length s-1] = ']' ->
                    let len = String.length s in
                    let tok1 =
                      Token(String.sub s 0 (len-1),
                            String.sub scap 0 (len-1)) in
                    if len > 1 then Queue.add tok1 q;
                    Queue.add (Token("]","]")) q;
                    Queue.add Space q;
                    found := true
               | _, CRLF ->
                    Queue.add tok q;
                    found := true
                    (* raise Exit *)
               | _ ->
                    if !prev_tok <> Incomplete then
                      Queue.add !prev_tok q;
                    prev_tok := tok
        )
        rs.toks
    with Exit -> ()
  );
  if !found then (
    Queue.clear rs.toks;
    Queue.transfer q rs.toks
  )


let is_number s =
  try
    for k = 0 to String.length s - 1 do
      match s.[k] with
        | '0'..'9' -> ()
        | _ -> raise Not_found
    done;
    true
  with Not_found -> false


let consume rs n =
  for k = 1 to n do
    ignore(Queue.pop rs.toks)
  done


let consume_crlf rs =
  let tok = Queue.pop rs.toks in
  if tok <> CRLF then raise Parse_error;
  ()


let string_of_tok tok =
  (* return the orginal string of a token *)
  match tok with
    | Lparen -> "("
    | Rparen -> ")"
    | Asterisk -> "*"
    | Percent -> "%"
    | Space -> " "
    | BS -> "\008"
    | Dquote -> "\""
    | Literal_header n -> sprintf "{%s\r\n" n
    | Literal lit -> lit#value
    | Control c -> String.make 1 c
    | Token(tok,_) -> tok
    | QText s -> Netimap_prelim.quote_string s
    | CRLF -> "\r\n"
    | End -> ""
    | EOF -> ""
    | Incomplete -> assert false
    | Error _ -> assert false



let rec space_separated_atoms toks =
  match toks with
    | (Token(_,tok) :: Space :: toks') ->
         tok :: space_separated_atoms toks'
    | [Token(_,tok)] ->
         [tok]
    | _ ->
         assert false  (* CHECK *)


let remove_parentheses toks =
  match toks with
    | Lparen :: toks' ->
         ( match List.rev toks' with
             | Rparen :: toks'' ->
                  toks''
             | _ -> raise Parse_error
         )
    | _ ->
         raise Parse_error


let parse_paren_list_1 ?(recursive=false) 
                       (f : int -> token list -> 'a)
                       (toks : token list) : ('a list * token list) =
  (* Production: Lparen [ item *(SP item) ] Rparen *)
  (* if recursive: inner Lparen is recognized. When f is called with Lparen
     as first token, it is guaranteed that f receives a well-parenthesized
     list. f can then call parse_paren_list on this list.
   *)
  let rec parse_1 pos current toks =
    match toks with
      | Space :: toks1 ->
           if current=[] then
             parse_1 pos [] toks1   (* ignore several spaces in sequence *)
           else
             let res_list, rem_toks = parse_1 (pos+1) [] toks1 in
             (f pos (List.rev current) :: res_list, rem_toks)
      | Lparen :: toks1 when recursive ->
           let p, pos1 =
             if current=[] then
               ([], pos)
             else
               ( [ f pos (List.rev current) ], pos+1) in
           let toks2, toks3 = split [Lparen] 1 toks1 in
           let res_list, rem_toks = parse_1 (pos1+1) [] toks3 in
           (p @ (f pos1 toks2 :: res_list), rem_toks)
      | Rparen :: toks1 -> 
           if current <> [] then
             ( [ f pos (List.rev current) ], toks1 )
           else
             ( [], toks1 )
      | tok :: toks1 ->
           parse_1 pos (tok :: current) toks1
      | [] ->
          raise Parse_error
   and split current n toks =
      match toks with
        | Rparen :: toks1 ->
             if n=1 then
               (List.rev (Rparen::current), toks1)
             else
               split (Rparen::current) (n-1) toks1
        | Lparen :: toks1 ->
             split (Lparen::current) (n+1) toks1
        | tok :: toks1 ->
             split (tok::current) n toks1
        | [] ->
             raise Parse_error
  in
  match toks with
    | Lparen :: toks1 ->
         parse_1 0 [] toks1
    | _ ->
         raise Parse_error


let parse_paren_list ?recursive f toks =
  let (l, remaining_toks) = parse_paren_list_1 ?recursive f toks in
  if remaining_toks <> [] then
    raise Parse_error;
  l


let parse_paren_list_as_pairs ?recursive
                              (f : int -> token list -> token list -> 'a)
                              (toks : token list) : 'a list =
  let store = ref None in
  let l =
    parse_paren_list
      ?recursive
      (fun i toks ->
         match !store with
           | None ->
                store := Some toks;
                []
           | Some old_toks ->
                let x = f (i lsr 1) old_toks toks in
                store := None;
                [x]
      )
      toks in
  if !store <> None then raise Parse_error;
  List.flatten l
               

let parse_msg_flag toks =
  match toks with
    | [ BS; Token(_,s) ] ->
         ( match s with
             | "ANSWERED" -> `Answered
             | "FLAGGED" -> `Flagged
             | "DELETED" -> `Deleted
             | "SEEN" -> `Seen
             | "DRAFT" -> `Draft
             | _ -> `System s
         )
    | [ Token(_,s) ] ->
         `Keyword s
    | _ -> raise Parse_error


let parse_msg_flag_or_recent toks =
  match toks with
    | [ BS; Token(_, "RECENT") ] -> `Recent
    | _ -> (parse_msg_flag toks :> ret_msg_flag)



let parse_mbx_flag toks =
  match toks with
    | [ BS; Token(_,s) ] ->
         ( match s with
             | "NOINFERIORS" -> `Noinferiors
             | "NOSELECT" -> `Noselect
             | "MARKED" -> `Marked
             | "UNMARKED" -> `Unmarked
             | _ -> `System s
         )
    | _ -> raise Parse_error
                 

let parse_mbox delim_opt s : mailbox =
  (* - Use the delimiter to split s
   * - decode UTF-7
   *)
  match delim_opt with
    | None ->  (* flat names *)
         [ mutf7_decode s ]
    | Some d ->
         let re = Netstring_str.regexp(Netstring_str.quote (String.make 1 d)) in
         let hier = Netstring_str.split_delim re s in
         List.map mutf7_decode hier

let parse_astring =
  function
  | Token(s,_) -> s
  | QText s -> s
  | Literal lit -> lit # value
  | _ -> raise Parse_error

let parse_nstring =
  function
  | Token(_, "NIL") -> None
  | QText s -> Some(Netimap_prelim.inline_literal s)
  | Literal lit -> Some lit
  | _ -> raise Parse_error

let parse_nstring_s tok =
  match parse_nstring tok with
    | None -> None
    | Some lit -> Some lit#value

let parse_string =
  function
  | QText s -> s
  | Literal lit -> lit#value
  | _ -> raise Parse_error


let parse_number =
  function
  | Token(num, _) when is_number num ->
       Int64.of_string num
  | _ ->
       raise Parse_error

let date_time_re =
  Netstring_str.regexp "^\\( [0-9]|[0-9][0-9]\\)-\\([A-Za-z]+\\)-\\([0-9][0-9][0-9][0-9]\\) \\([0-9][0-9]\\):\\([0-9][0-9]\\):\\([0-9][0-9]\\) \\([-+]\\)\\([0-9][0-9][0-9][0-9]\\)$"

let parse_month =
  function
  | "Jan" -> 1
  | "Feb" -> 2
  | "Mar" -> 3
  | "Apr" -> 4
  | "May" -> 5
  | "Jun" -> 6
  | "Jul" -> 7
  | "Aug" -> 8
  | "Sep" -> 9
  | "Oct" -> 10
  | "Nov" -> 11
  | "Dec" -> 12
  | _ -> raise Parse_error


let parse_date_time s =
  match Netstring_str.string_match date_time_re s 0 with
    | None ->
        raise Parse_error
    | Some m ->
        let day_s = Netstring_str.matched_group m 1 s in
        let day = 
          if day_s.[0] = ' ' then 
            int_of_string(String.sub day_s 1 1) 
          else
            int_of_string day_s in
        let month_s = Netstring_str.matched_group m 2 s in
        let month = parse_month month_s in
        let year_s = Netstring_str.matched_group m 3 s in
        let year = int_of_string year_s in
        let hour_s = Netstring_str.matched_group m 4 s in
        let hour = int_of_string hour_s in
        let minute_s = Netstring_str.matched_group m 5 s in
        let minute = int_of_string minute_s in
        let second_s = Netstring_str.matched_group m 6 s in
        let second = int_of_string second_s in
        let zone_sign_s = Netstring_str.matched_group m 7 s in
        let zone_sign = if zone_sign_s = "+" then 1 else (-1) in
        let zone_s = Netstring_str.matched_group m 8 s in
        let zone = int_of_string zone_s in
        { Netdate.year; month; day; hour; minute; second;
          nanos = 0;
          zone = zone_sign * ((zone/100) * 60 + (zone mod 100));
          week_day = (-1)
        }
          


let single_tok toks =
  match toks with
    | [ tok ] -> tok
    | _ -> raise Parse_error


let parse_mailbox_string tok =
  let s = parse_astring tok in
  if ascii_uppercase s = "INBOX" then
    "INBOX"
  else
    s

let parse_address toks =
  (* "(" addr-name addr-adl addr-mailbox addr-host ")" *)
  match toks with
    | [ Lparen; tok1; Space; tok2; Space; tok3; Space; tok4; Rparen ] ->
         let display_opt = parse_nstring_s tok1 in
         let display =
           match display_opt with
             | None -> ""
             | Some d -> d in
         let _route_opt = parse_nstring tok2 in (* ignored *)
         let mbox_opt = parse_nstring_s tok3 in
         let host_opt = parse_nstring_s tok4 in
         ( match mbox_opt, host_opt with
             | Some mbox, Some host ->
                  (* CHECK: how do imap servers return addresses without
                     domain? This case is not covered by the RFC
                   *)
                  let host_opt = if host = "" then None else Some host in
                  `Mbox(display,mbox,host_opt)
             | Some group, None ->
                  `Group_start group
             | None, None ->
                  `Group_end
             | None, Some _ ->
                  raise Parse_error
         )
    | _ ->
         raise Parse_error

let postprocess_addresses addrs =
  let rec gather group_opt addrs =
    match addrs with
      | `Mbox(display, mbox, host) :: addrs' ->
           let m = (display, `Mailbox(mbox, host)) in
           ( match group_opt with
               | None ->
                    m :: gather None addrs'
               | Some (group_name, group_members) ->
                    gather (Some(group_name, m::group_members)) addrs'
           )
      | `Group_start group_name :: addrs' ->
           if group_opt <> None then raise Parse_error;
           gather (Some(group_name,[])) addrs'
      | `Group_end :: addrs' ->
           ( match group_opt with
               | None -> raise Parse_error
               | Some (group_name, members) ->
                    (* according to rfc-2822, the group name is a display
                       name *)
                    (group_name, `Group (List.rev members)) :: 
                      gather None addrs'
           )
      | [] ->
           if group_opt <> None then raise Parse_error;
           [] in
  gather None addrs


let parse_paren_addresses toks =
  (* "(" 1*address ")" *)
  postprocess_addresses
    (parse_paren_list
       ~recursive:true
       (fun _ toks2 -> parse_address toks2)
       toks
    )


let parse_envelope toks =
  let env_date = ref None in
  let env_subject = ref None in
  let env_from = ref [] in
  let env_sender = ref [] in
  let env_reply_to = ref [] in
  let env_to = ref [] in
  let env_cc = ref [] in
  let env_bcc = ref [] in
  let env_in_reply_to = ref None in
  let env_message_id = ref None in
  ignore
    (parse_paren_list
       ~recursive:true
       (fun k toks1 ->
        match k with
          | 0 ->
               ( match toks1 with
                   | [ tok ] -> env_date := parse_nstring_s tok
                   | _ -> raise Parse_error
               )
          | 1 ->
               ( match toks1 with
                   | [ tok ] -> env_subject := parse_nstring_s tok
                   | _ -> raise Parse_error
               )
          | 2 ->
               ( match toks1 with
                   | [ Token(_, "NIL") ] -> ()
                   | Lparen :: _ -> env_from := parse_paren_addresses toks1
                   | _ -> raise Parse_error
               )
          | 3 ->
               ( match toks1 with
                   | [ Token(_, "NIL") ] -> ()
                   | Lparen :: _ -> env_sender := parse_paren_addresses toks1
                   | _ -> raise Parse_error
               )
          | 4 ->
               ( match toks1 with
                   | [ Token(_, "NIL") ] -> ()
                   | Lparen :: _ -> env_reply_to := parse_paren_addresses toks1
                   | _ -> raise Parse_error
               )
          | 5 ->
               ( match toks1 with
                   | [ Token(_, "NIL") ] -> ()
                   | Lparen :: _ -> env_to := parse_paren_addresses toks1
                   | _ -> raise Parse_error
               )
          | 6 ->
               ( match toks1 with
                   | [ Token(_, "NIL") ] -> ()
                   | Lparen :: _ -> env_cc := parse_paren_addresses toks1
                   | _ -> raise Parse_error
               )
          | 7 ->
               ( match toks1 with
                   | [ Token(_, "NIL") ] -> ()
                   | Lparen :: _ -> env_bcc := parse_paren_addresses toks1
                   | _ -> raise Parse_error
               )
          | 8 ->
               ( match toks1 with
                   | [ tok ] -> env_in_reply_to := parse_nstring_s tok
                   | _ -> raise Parse_error
               )
          | 9 ->
               ( match toks1 with
                   | [ tok ] -> env_message_id := parse_nstring_s tok
                   | _ -> raise Parse_error
               )
          | _ ->
               raise Parse_error
       )
       toks
    );
  { env_date = !env_date;
    env_subject = !env_subject;
    env_from = !env_from;
    env_sender = !env_sender;
    env_reply_to = !env_reply_to;
    env_to = !env_to;
    env_cc = !env_cc;
    env_bcc = !env_bcc;
    env_in_reply_to = !env_in_reply_to;
    env_msg_id = !env_message_id;
  }


let rec parse_body toks =
  let case = ref `Start in
  let offset = ref 0 in
  let media_type = ref "" in
  let media_subtype = ref "" in
  let media_type_params = ref [] in
  let disposition_main = ref "" in
  let disposition_params = ref [] in
  let language = ref [] in
  let location = ref None in
  let id = ref None in
  let description = ref None in
  let encoding = ref "" in
  let md5 = ref None in
  let octets = ref 0L in
  let lines = ref None in
  let env = ref None in

  let sub_bodies = ref [] in

  let refine_start_case() =
    match String.lowercase !media_type, String.lowercase !media_subtype with
      | "message", "rfc822" ->
           case := `Message
      | "text", _ ->
           case := `Text
      | _ ->
           case := `Basic
  in

  let parse_body_fld_param toks =
    match toks with
      | [ Token(_, "NIL") ] ->
           []
      | _ ->
           parse_paren_list_as_pairs
             (fun _ toks1 toks2 ->
                let name = parse_astring (single_tok toks1) in
                let value = parse_astring (single_tok toks2) in
                (name, value)
             )
             toks
  in

  let parse_body_fld_dsp toks =
    ignore(
        parse_paren_list
          ~recursive:true
          (fun k toks1 ->
             match k with
               | 0 ->
                    disposition_main := parse_astring (single_tok toks1)
               | 1 ->
                    disposition_params := parse_body_fld_param toks1
               | _ ->
                    raise Parse_error
          )
          toks
      )
  in

  let parse_body_fld_lang toks =
    match toks with
      | [ tok ] ->
           ( match parse_nstring_s tok with
               | None ->
                    language := []
               | Some lang ->
                    language := [lang]
           )
      | Lparen :: _ ->
           language :=
             parse_paren_list
               (fun _ toks -> parse_astring (single_tok toks))
               toks
      | _ ->
           raise Parse_error
  in

  let parse_body_fields k toks =
    match k with
      | 0 ->
           media_type_params := parse_body_fld_param toks
      | 1 ->
           id := parse_nstring_s (single_tok toks)
      | 2 ->
           description := parse_nstring_s (single_tok toks)
      | 3 ->
           encoding := parse_astring (single_tok toks)
      | 4 ->
           octets := parse_number (single_tok toks)
      | _ ->
           raise Parse_error
  in

  ignore
    (parse_paren_list
       ~recursive:true
       (fun k toks ->
          match !case with
            | `Start ->
                 (* Parsing the media type, or first multipart body *)
                 ( match k with
                     | 0 ->
                          ( match toks with
                              | [ QText main ] ->
                                   media_type := main
                              | [ Literal lit ] ->
                                   media_type := lit#value
                              | Lparen :: _ ->
                                   media_type := "multipart";
                                   sub_bodies := [ parse_body toks ];
                                   case := `Multipart
                              | _ ->
                                   raise Parse_error
                          )
                     | 1 ->
                          ( match toks with
                              | [ tok ] ->
                                   media_subtype := parse_string tok;
                                   offset := 2;
                                   refine_start_case()
                              | _ ->
                                   raise Parse_error
                          )
                     | _ -> assert false
                 )
            | `Multipart ->
                 ( match toks with
                     | Lparen :: _ ->
                          sub_bodies := parse_body toks :: !sub_bodies
                     | [ (QText _ | Literal _) as tok ] ->
                          media_subtype := parse_string tok;
                          case := `Multipart_ext;
                          offset := k+1
                     | _ ->
                          raise Parse_error
                 )
            | `Multipart_ext ->
                 ( match k - !offset with
                     | 0 ->
                          media_type_params := parse_body_fld_param toks
                     | 1 ->
                          parse_body_fld_dsp toks
                     | 2 ->
                          parse_body_fld_lang toks
                     | 3 ->
                          location := parse_nstring_s(single_tok toks)
                     | _ ->
                          (* body-extension *)
                          ()
                 )
            | `Basic ->
                 parse_body_fields (k - !offset) toks;
                 if k - !offset = 4 then (
                   case := `Ext;
                   offset := k+1
                 )
                   
            | `Text ->
                 if k - !offset < 5 then
                   parse_body_fields (k - !offset) toks
                 else
                   if k - !offset = 5 then (
                     lines := Some(parse_number (single_tok toks));
                     case := `Ext;
                     offset := k+1
                   )
                   else
                     raise Parse_error
            | `Message ->
                 if k - !offset < 5 then
                   parse_body_fields (k - !offset) toks
                 else (
                   match k - !offset with
                     | 5 -> (* envelope *)
                          env := Some (parse_envelope toks)
                     | 6 -> (* body *)
                          sub_bodies := [ parse_body toks ]
                     | 7 -> (* body-fld-lines *)
                          lines := Some(parse_number (single_tok toks));
                          case := `Ext;
                          offset := k+1
                     | _ -> raise Parse_error
                 )
            | `Ext ->
                 ( match k - !offset with
                     | 0 ->
                          media_type_params := parse_body_fld_param toks
                     | 1 ->
                          parse_body_fld_dsp toks
                     | 2 ->
                          parse_body_fld_lang toks
                     | 3 ->
                          location := parse_nstring_s(single_tok toks)
                     | 4 ->
                          (* body-extension *)
                          ()
                     | _ ->
                          raise Parse_error
                 )
       )
       toks
    );
  let get_core() =
    { bp_media_type = (!media_type,
                       !media_subtype,
                       !media_type_params);
      bp_disposition = (!disposition_main,
                        !disposition_params);
      bp_language = !language;
      bp_location = !location
    } in
  match !case with
    | `Multipart | `Multipart_ext ->
         let core = get_core() in
         `Parts(core, List.rev !sub_bodies)
    | _ ->
         let body =
           { bp_core = get_core();
             bp_id = !id;
             bp_description = !description;
             bp_encoding = !encoding;
             bp_md5 = !md5;
             bp_octets = !octets;
             bp_lines = !lines;
             bp_envelope = !env;
           } in
         `Body body



let rec section_case ~allow_mime l =
  match l with
    | [ "HEADER" ] -> `Header
    | [ "HEADER"; "FIELDS" ] -> `Header_fields []
    | [ "HEADER"; "FIELDS"; "NOT" ] -> `Header_fields_not []
    | [ "TEXT" ] -> `Text
    | [ "MIME" ] when allow_mime -> `Mime
    | s :: l' when is_number s ->
        let p = int_of_string s in
        if l' = [] then
          `Sub(p, `Part)
        else
          `Sub(p, section_case ~allow_mime:true l')
    | _ -> raise Parse_error


let parse_body_fetch_1 section toks =
  (* ["<" number ">"] SP nstring *)
  match toks with
    | [ Token(_,bytes); Space; nstring; ] 
          when bytes.[0] = '<'
               && bytes.[String.length bytes-1] = '>'
               && is_number(String.sub bytes 1 (String.length bytes-2)) ->
        let bytes_i = 
          Int64.of_string (String.sub bytes 1 (String.length bytes-2)) in
        let nstr = parse_nstring nstring in
        (section, Some bytes_i, nstr)
    | [ Space; nstring ] ->
        let nstr = parse_nstring nstring in
        (section, None, nstr)
    | _ ->
         raise Parse_error


let dot_re = Netstring_str.regexp "[.]"

let parse_body_fetch toks =
  (* section ["<" number ">"] SP nstring *)
  let toks = isolate_brackets toks in
  match toks with
    | Token(_,"[") :: Token(_,"]") :: toks1 ->
        parse_body_fetch_1 `Part toks1
    | Token(_,"[") :: Token(_,section_tok) :: toks1 ->
        let l = Netstring_str.split dot_re section_tok in
        let case = section_case ~allow_mime:false l in
        ( match case with
            | `Header_fields _
            | `Header_fields_not _ ->
                ( match toks1 with
                    | Space :: toks2 ->
                        let (names, toks3) =
                          parse_paren_list_1
                            (fun _ itoks ->
                               match itoks with
                                 | [ astring ] -> parse_astring astring
                                 | _ -> raise Parse_error
                            )
                            toks2 in
                        let section =
                          match case with
                            | `Header_fields _ -> `Header_fields names
                            | `Header_fields_not _ -> `Header_fields_not names
                            | _ -> assert false in
                        ( match toks3 with
                            | Token(_,"]") :: toks4 ->
                                parse_body_fetch_1 section toks4
                            | _ ->
                                raise Parse_error
                        )
                    | _ ->
                        raise Parse_error
                )
            | _ ->
                ( match toks1 with
                    | Token(_,"]") :: toks2 ->
                        parse_body_fetch_1 case toks2
                    | _ ->
                        raise Parse_error
                )
        )
    | _ ->
        raise Parse_error
        


let reset rs =
  rs.recognize_literal <- false

let restart f rs tok =
  Queue.add tok rs.toks;
  f rs

let rec after ntoks f_continue rs : rstate =
  (* run f_continue when we have at least ntoks tokens *)
  if Queue.length rs.toks < ntoks then
    `C(rs, restart (after ntoks f_continue))
  else
    f_continue rs

let rec after_these these f_continue rs : rstate =
  (* run f_continue when [these] tokens have been found *)
  match queue_contains rs these with
    | None ->
         ( match queue_contains rs [ CRLF ] with
             | None ->
                  `C(rs, restart (after_these these f_continue))
             | Some _ ->
                  raise Parse_error
         )
    | Some i ->
         f_continue i rs


let rec after_rbracket_space f_continue rs : rstate =
  (* run f_continue when "] " has been found *)
  isolate_rbracket_space rs;
  match queue_contains rs [ Token("]","]"); Space ]  with
    | None ->
         ( match queue_contains rs [ CRLF ]  with
             | None ->
                  `C(rs, restart (after_rbracket_space f_continue))
             | Some _ ->
                  raise Parse_error
         )
    | Some i ->
         f_continue i rs


let rec process_resp_text
          : ( 'a -> rstate ) -> (resp_text_code -> string -> 'a ) -> 
                   int -> rstate_details -> rstate
  = fun cont f ntoks rs ->
  (* Production: resp-text CRLF *)
  after
    ntoks
    (fun rs ->
       match prefix rs 1 with
         | [ Token(_, "[PARSE") ] ->
              consume rs 2;
              process_resp_code_2 cont `Parse f rs
         | [ Token(_, "[ALERT") ] ->
              consume rs 2;
              process_resp_code_2 cont `Alert f rs
         | [ Token(_, "[READ-ONLY") ] ->
              consume rs 2;
              process_resp_code_2 cont `Read_only f rs
         | [ Token(_, "[READ-WRITE") ] ->
              consume rs 2;
              process_resp_code_2 cont `Read_write f rs
         | [ Token(_, "[TRYCREATE") ] ->
              consume rs 2;
              process_resp_code_2 cont `Try_create f rs
         | [ Token(_, "[UIDNEXT") ] ->
              consume rs 2;
              process_resp_code_3 cont (fun n -> `Uidnext n) f rs
         | [ Token(_, "[UIDVALIDITY") ] ->
              consume rs 2;
              process_resp_code_3 cont (fun n -> `Uidvalidity n) f rs
         | [ Token(_, "[UNSEEN") ] ->
              consume rs 2;
              process_resp_code_3 cont (fun n -> `Unseen n) f rs
         | [ Token(_, "[CAPABILITY") ] ->
              consume rs 2;
              process_resp_code_capability cont f rs
         | [ Token(_, "[BADCHARSET") ] ->
              consume rs 2;
              process_resp_code_badcharset cont f rs
         | [ Token(_, "[PERMANENTFLAGS") ] ->
              consume rs 2;
              process_resp_code_permanentflags cont f rs
         | [ Token(s,_) ] ->
              if s.[0] = '[' then (
                (* Unknown code: map to `None *)
                consume rs 2;
                process_resp_code_1 cont (fun _ -> `None) f rs
              )
              else
                let buffer = Buffer.create 80 in
                process_text cont buffer `None f rs
         | _ ->
              let buffer = Buffer.create 80 in
              process_text cont buffer `None f rs
    )
    rs

and process_resp_code_1 cont f_code f rs =
  after_rbracket_space
    (fun i rs ->
       let toks = prefix rs i in
       let code = f_code toks in
       consume rs (i+2);   (* the two extra: "[" and space *)
       let buffer = Buffer.create 80 in
       process_text cont buffer code f rs
    )  
    rs

and process_resp_code_2 cont code f rs =
  process_resp_code_1
    cont
    (fun toks ->
       if toks <> [] then raise Parse_error;
       code
    )
    f rs  

and process_resp_code_3 cont f_code f rs =
  process_resp_code_1
    cont
    (fun toks ->
       match toks with
         | [ Token(number,_) ] when is_number number ->
              let n = Int64.of_string number in
              f_code n
         | _ ->
              raise Parse_error
    )
    f rs  

and process_resp_code_capability cont f rs =
  process_resp_code_1
    cont
    (fun toks ->
       match toks with
         | Space :: toks1 ->
              let caps = space_separated_atoms toks1 in
              `Capability caps
         | _ ->
              raise Parse_error
    )
    f rs


and process_resp_code_badcharset cont f rs =
  rs.recognize_literal <- true;
  process_resp_code_1
    cont
    (fun toks ->
       match toks with
         | Space :: (Lparen :: _ as toks1) ->
              let csinfo =
                parse_paren_list
                  (fun i toks ->
                     match single_tok toks with
                       | (Token _ | QText _ | Literal _) as tok ->
                            parse_astring tok
                       | _ ->
                            raise Parse_error
                  )
                  toks1 in
              `Bad_charset csinfo
         | [] ->
              `Bad_charset []
         | _ ->
              raise Parse_error
    )
    f rs


and process_resp_code_permanentflags cont f rs =
  process_resp_code_1
    cont
    (fun toks ->
       match toks with
         | Space :: toks1 ->
              let flags =
                parse_paren_list (fun i toks2 -> parse_msg_flag toks2) toks1 in
              `Permanentflags flags
         | _ ->
              raise Parse_error
    )
    f rs


and process_text cont buffer code f rs =
  (* Production: text CRLF *)
  rs.recognize_literal <- false;
  after
    1
    (fun rs ->
       let tok = Queue.take rs.toks in
       if tok <> CRLF then (
         let s = string_of_tok tok in
         Buffer.add_string buffer s;
         process_text cont buffer code f rs
       )
       else (
         let text = Buffer.contents buffer in
         reset rs;
         let arg = f code text in
         cont arg
       )
    )
    rs
           


let rec process_phrase r_untagged (rs:rstate_details) =
  (* Production:
      | *(continue-req | response-data) response-done
      | greeting
   *)
  after
    1
    (fun rs ->
       match prefix rs 1 with
         | [ Asterisk ] ->
              consume rs 1;
              `C(rs, restart (process_untagged 2 r_untagged))
         | [ Token("+",_) ] ->
              consume rs 1;
              `C(rs, restart (process_continuation r_untagged))
         | [ Token(tag,_) ] ->
              consume rs 1;
              `C(rs, restart (process_tagged tag r_untagged))
         | [ EOF ] ->
              consume rs 1;
              let r =
                { r_untagged = List.rev r_untagged;
                  r_tag = "";
                  r_state = `EOF
                } in
              `R r
         | _ ->
              raise Parse_error
    )
    rs

and process_continuation r_untagged rs : rstate =
  (* production: continue-req *)
  if rs.auth_in_progress then
    after 3
      (fun rs ->
         match prefix rs 3 with
           | [ Space; Token(enc_data, _); CRLF ] ->
               consume rs 3;
                let data =
                  try
                    Netencoding.Base64.decode ~url_variant:false enc_data
                  with
                    | Failure _ -> raise Parse_error in
                process_phrase (`Authenticate_continue data :: r_untagged) rs
           | _ ->
                raise Parse_error
      )
      rs
  else
    after_these
      [CRLF]
      (fun i rs ->
         let toks = prefix rs i in
         consume rs i;
         consume_crlf rs;
         let s = String.concat "" (List.map string_of_tok toks) in
         process_phrase (`Command_continue s :: r_untagged) rs
      )
      rs

and process_tagged tag r_untagged rs =
  (* production: response-tagged *)
  after
    3
    (fun rs ->
       match prefix rs 3 with
         | [ Space; Token(_, "BYE"); Space ] ->
              consume rs 3;
              process_resp_text_and_done
                tag r_untagged
                (fun code text -> `Bye(code,text))
                rs
         | [ Space; Token(_, "OK"); Space ] ->
              consume rs 3;
              process_resp_text_and_done
                tag r_untagged
                (fun code text -> `State(`OK,code,text))
                rs
         | [ Space; Token(_, "NO"); Space ] ->
              consume rs 3;
              process_resp_text_and_done
                tag r_untagged
                (fun code text -> `State(`NO,code,text))
                rs
         | [ Space; Token(_, "BAD"); Space ] ->
              consume rs 3;
              process_resp_text_and_done
                tag r_untagged
                (fun code text -> `State(`BAD,code,text))
                rs
         | _ ->
              raise Parse_error
    )
    rs


and process_untagged ntoks r_untagged rs =
  (* production: response-data *)
  after
    ntoks
    (fun rs ->
       match prefix rs 3 with
         | [ Space; Token(_, "PREAUTH"); Space ] ->
              consume rs 3;
              process_resp_text_and_restart
                (fun code text -> `Preauth(code,text) :: r_untagged)
                rs
         | [ Space; Token(_, "BYE"); Space ] ->
              consume rs 3;
              process_resp_text_and_done
                (fun code text -> `Bye(code,text) :: r_untagged)
                rs
         | [ Space; Token(_, "OK"); Space ] ->
              consume rs 3;
              process_resp_text_and_restart
                (fun code text -> `State(`OK,code,text) :: r_untagged)
                rs
         | [ Space; Token(_, "NO") ] ->
              consume rs 3;
              process_resp_text_and_restart
                (fun code text -> `State(`NO,code,text) :: r_untagged)
                rs
         | [ Space; Token(_, "BAD") ] ->
              consume rs 3;
              process_resp_text_and_restart
                (fun code text -> `State(`BAD,code,text) :: r_untagged)
                rs
         | [ Space; Token(_, "FLAGS"); Space ] ->
              consume rs 3;
              process_resp_flags r_untagged rs
         | [ Space; Token(_, "LIST"); Space ] ->
              consume rs 3;
              process_resp_list (fun data -> `List data) r_untagged rs
         | [ Space; Token(_, "LSUB"); Space ] ->
              consume rs 3;
              process_resp_list (fun data -> `Lsub data) r_untagged rs
         | [ Space; Token(_, "SEARCH"); Space ] ->
              consume rs 3;
              process_resp_search r_untagged rs
         | [ Space; Token(_, "SEARCH"); CRLF ] ->
              consume rs 3;
              process_phrase (`Search [] :: r_untagged) rs
         | [ Space; Token(_, "STATUS"); Space ] ->
              consume rs 3;
              process_resp_status r_untagged rs
         | [ Space; Token(_, "CAPABILITY"); Space ] ->
              consume rs 3;
              process_resp_capability r_untagged rs
         | [ Space; Token(num, _); Space ] when is_number num ->
              if ntoks < 5 then
                process_untagged 5 r_untagged rs
              else
                ( match prefix ~skip:3 rs 2 with
                    | [ Token(_, "EXPUNGE"); CRLF ] ->
                         consume rs 5;
                         let n = Int64.of_string num in
                         process_phrase (`Expunge n :: r_untagged) rs
                    | [ Token(_, "FETCH"); Space ] ->
                         consume rs 5;
                         let n = Int64.of_string num in
                         process_fetch n r_untagged rs
                    | [ Token(_, "EXISTS"); CRLF ] ->
                         consume rs 5;
                         let n = Int64.of_string num in
                         process_phrase (`Exists n :: r_untagged) rs
                    | [ Token(_, "RECENT"); CRLF ] ->
                         consume rs 5;
                         let n = Int64.of_string num in
                         process_phrase (`Recent n :: r_untagged) rs
                    | _ -> raise Parse_error
                )
         | _ -> raise Parse_error
    )
    rs

and process_resp_text_and_restart f rs =
  process_resp_text
    (fun r_untagged -> process_phrase r_untagged rs)
    f 1 rs

and process_resp_text_and_done tag r_untagged f rs =
  process_resp_text
    (fun (rcond:response_cond) ->
       after 1
         (fun rs ->
            if prefix rs 1 = [ EOF ] then
              `R { r_untagged;
                   r_tag = tag;
                   r_state = (rcond :> response_cond_or_eof);
                 }
            else
              raise Parse_error
         )
         rs
    )
    f 1 rs
    

and process_resp_flags r_untagged rs =
  after_these
    [ CRLF ]
    (fun i rs ->
       let toks = prefix rs i in
       let flags =
         parse_paren_list (fun _ -> parse_msg_flag) toks in
       consume rs i;
       consume_crlf rs;
       process_phrase
         (`Flags flags :: r_untagged) rs
    )
    rs

and process_resp_list f r_untagged rs =
  rs.recognize_literal <- true;
  after_these
    [ CRLF ]
    (fun i rs ->
       let toks = prefix rs i in
       let p_toks = [ Lparen ] @ toks @ [ Rparen ] in
       let l =
         parse_paren_list
           ~recursive:true
           (fun i toks ->
              match i with
                | 0 ->
                     `E0(parse_paren_list (fun _ -> parse_mbx_flag) toks)
                | 1 ->
                     ( match toks with
                         | [ QText delim ] ->
                              if String.length delim <> 1 then
                                raise Parse_error;
                              `E1 (Some delim.[0])
                         | [ Token(_, "NIL") ] ->
                              `E1 None
                         | _ ->
                              raise Parse_error
                     )
                | 2 -> 
                     ( match toks with
                         | [ (Token _ | QText _ | Literal _) as tok ] ->
                              `E2 (parse_mailbox_string tok)
                         | _ ->
                              raise Parse_error
                     )
                | _ -> raise Parse_error
           )
           p_toks in
       match l with
         | [ `E0 flags; `E1 delim_opt; `E2 mbox ] ->
              let p = f (flags, delim_opt, parse_mbox delim_opt mbox) in
              consume rs i;
              consume_crlf rs;
              process_phrase (p::r_untagged) rs
         | _ ->
              raise Parse_error
    )
    rs

and process_resp_search r_untagged rs =
  after_these
    [ CRLF ]
    (fun i rs ->
       let toks = prefix rs i in
       let l1 = space_separated_atoms toks in
       let l2 =
         List.map
           (fun s ->
              if not (is_number s) then raise Parse_error;
              Int64.of_string s
           )
           l1 in
       consume rs i;
       consume_crlf rs;
       process_phrase (`Search l2 :: r_untagged) rs
    )
    rs


and process_resp_capability r_untagged rs =
  after_these
    [ CRLF ]
    (fun i rs ->
       let toks = prefix rs i in
       let l1 = space_separated_atoms toks in
       consume rs i;
       consume_crlf rs;
       process_phrase (`Capability l1 :: r_untagged) rs
    )
    rs


and process_resp_status r_untagged rs =
  rs.recognize_literal <- true;
  after_these
    [ CRLF ]
    (fun i rs ->
       let toks = prefix rs i in
       match toks with
         | (Token _ | QText _ | Literal _) as tok ::
           Space :: ((Lparen :: _) as plist) ->
              let mbox =
                (parse_mailbox_string tok, None) in
              let status =
                parse_paren_list_as_pairs
                  (fun i toks1 toks2 ->
                     let tag =
                       match toks1 with
                         | [ Token(_, "MESSAGES") ] -> `Messages
                         | [ Token(_, "RECENT") ] -> `Recent
                         | [ Token(_, "UIDNEXT") ] -> `Uidnext
                         | [ Token(_, "UIDVALIDITY") ] -> `Uidvalidity
                         | [ Token(_, "UNSEEN") ] -> `Unseen
                         | _ -> raise Parse_error in
                     let value =
                       match toks2 with
                         | [ Token(s, _) ] when is_number s -> Int64.of_string s
                         | _ -> raise Parse_error in
                     (tag, value)
                  )
                  plist in
              consume rs i;
              consume_crlf rs;
              process_phrase (`Status(mbox,status) :: r_untagged) rs
         | _ -> raise Parse_error
    )
    rs

and gather_fetch_msg_att rs toks =
  match toks with
    | Token(_, "FLAGS") :: Space :: toks1 ->
         let flags =
           parse_paren_list
             (fun i -> parse_msg_flag_or_recent)
             toks1 in
         `Flags flags

    | Token(_, "ENVELOPE") :: Space :: toks1 ->
        let env = parse_envelope toks1 in
        `Envelope env

    | Token(_, "INTERNALDATE") :: Space :: QText date_time :: [] ->
        let dt = parse_date_time date_time in
        `Internaldate dt

    | Token(_, "RFC822") :: Space :: tok :: [] ->
        let nstring = parse_nstring tok in
        `Rfc822 nstring

    | Token(_, "RFC822.HEADER") :: Space :: tok :: [] ->
        let nstring = parse_nstring tok in
        `Rfc822_header nstring

    | Token(_, "RFC822.TEXT") :: Space :: tok :: [] ->
        let nstring = parse_nstring tok in
        `Rfc822_text nstring

    | Token(_, "RFC822.SIZE") :: Space :: Token(num,_) :: [] ->
        if not(is_number num) then raise Parse_error;
        let n = Int64.of_string num in
        `Rfc822_size n

    | Token(_, "BODY") :: Space :: toks1 ->
        let body = parse_body toks1 in
        `Bodystructure body

    | Token(_, "BODYSTRUCTURE") :: Space :: toks1 ->
        let body = parse_body toks1 in
        `Bodystructure body

    | Token(_, "UID") :: Space :: uniqueid :: _ ->
        let uid = parse_number uniqueid in
        `Uid uid

    | Token(n1, n2) :: toks1 when String.length n2 >= 5 && 
                                    String.sub n2 0 5 = "BODY[" ->
        let n1 = String.sub n1 4 (String.length n1 - 4) in
        let n2 = ascii_uppercase n1 in
        let t1 = Token(n1,n2) in
        let section, offset_opt, data_opt =
          parse_body_fetch (t1::toks1) in
        `Body(section, offset_opt, data_opt)

    | _ -> raise Parse_error


and process_fetch num r_untagged rs =
  after_these
    [ CRLF ]
    (fun i rs ->
       let toks = prefix rs i in
       let atts = 
         parse_paren_list
           ~recursive:true
           (fun k toks1 ->
              gather_fetch_msg_att rs toks1
           )
           toks in
       let r = `Fetch(num, atts) in
       consume rs i;
       consume_crlf rs;
       process_phrase (r :: r_untagged) rs
    )    
    rs


let init_rstate rs =
  `C(rs, restart(process_phrase []))


let process rstate tok =
  match rstate with
    | `R _ -> failwith "too many tokens"
    | `C(rs, f) -> f rs tok


let have_result =
  function
  | `R response -> true
  | `C _ -> false


let test ~auth_in_progress (toks : token list) =
  (* For unit testing: parse [toks] sequence *)
  let rs =
    { toks = Queue.create();
      recognize_literal = true;
      auth_in_progress
    } in
  let toks = ref toks in
  let rstate = ref (init_rstate rs) in
  while not (have_result !rstate) do
    if !toks = [] then failwith "Empty token list";
    let tok = List.hd !toks in
    toks := List.tl !toks;
    rstate := process !rstate tok
  done;
  if !toks <> [] then failwith "Unprocessed tokens remain";
  match !rstate with
    | `R response -> response
    | `C _ -> assert false


let test_s ~auth_in_progress s =
  let lexbuf = Lexing.from_string s in
  let toks = ref [] in
  ( try
      let tok = ref (Netimap_lexer.token lexbuf) in
      while !tok <> End do
        toks := !tok :: !toks;
        tok := Netimap_lexer.token lexbuf
      done;
    with
      | _ -> failwith "Lexer error"
  );
  test ~auth_in_progress (List.rev !toks @ [ EOF ])
