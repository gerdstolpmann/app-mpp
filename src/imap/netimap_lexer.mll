(* $Id$ *)

(* See RFC-3501 *)

(* Lexical structure. This is a big mess in the IMAP standard.
   Depending on the context, different classes of characters 
   form the current "token".

   General context:
   - Atoms consist of:
     any except '(', ')', '{', Space, Control, '*', '%', '"', '\\', ']'
   - '"' starts a quoted string
   - '{' starts a literal
   - '(' starts a parenthesized list

   Astrings, however, permit ']' inside atoms.

   Characters that are special in certain contexts only:

   - "+" starts a continue-req. We can handle such a "+" as atom.
   - Inside fetch-att, there are a couple of special characeters.
     This is handled by a second parser.
   - "[" is sometimes special in responses

   Solution: Both "[" and "]" are always special. This means that atoms
   and astrings may consist of several tokens. Concatenating such tokens
   is left to the next upper parsing layer.

   Spaces are reported as such.

   This lexer is usually run on an incomplete string. Because of this,
   a token followed by EOF is considered invalid, and reported as Incomplete.
   The next upper parsing layer needs to increase the string buffer, and
   restart lexing.

   Bugs:
    - We don't support the "text" production correctly. At this point,
      we must not recognize quoted strings or literals, but we actually
      do. Let's hope IMAP servers play nice and don't send texts that
      can be misunderstood.

 *)

{
  open Netimap_prelim
}


rule token = parse
  "(" { Lparen }
| ")" { Rparen }
| " " { Space }
| "*" { Asterisk }
| "%" { Percent }
| "\\" { BS }
| '"' { Dquote }
| "{" ( [ '0'-'9' ]+ as n ) "}\013\010" { Literal_header n }
| "{" eof { Incomplete }
| "{" ['0'-'9']+ eof { Incomplete }
| "{" ['0'-'9']+ "}" eof { Incomplete }
| "{" ['0'-'9']+ "}\013" eof { Incomplete }
| '\013' '\010' { CRLF }
| '\013' eof { Incomplete }
| [ '\001'-'\009' '\011' '\012' '\014'-'\031' ] as c { Control c }
| [^ '(' ')' '*' '%' '\\' '"' '{' ' ' '\000'-'\031' ]+ as s 
     { Token(s, ascii_uppercase s) }
| [^ '(' ')' '*' '%' '\\' '"' '{' ' ' '\000'-'\031' ]+ eof { Incomplete }
| _ as c { Error ("Bad character: " ^ String.make 1 c) }
| eof { End }


and dquoted = parse
  "\\\\" { QText "\\" }
| "\\\"" { QText "\"" }
| [^ '\\' '"' '\000']+ as s { QText s }
| '"'    { Dquote }
| '\\' eof { Incomplete }
| _ as c { Error ("Bad character: " ^ String.make 1 c) }
